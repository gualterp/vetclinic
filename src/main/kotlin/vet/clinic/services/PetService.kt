package vet.clinic.services

import org.springframework.stereotype.Service
import vet.clinic.exceptions.NotFoundException
import vet.clinic.exceptions.PreconditionFailedException
import vet.clinic.model.AppointmentDAO
import vet.clinic.model.AppointmentRepository
import vet.clinic.model.PetDAO
import vet.clinic.model.PetRepository

@Service
class PetService (val pets: PetRepository,
                  val appointments: AppointmentRepository)
{

    fun getAllPets(): List<PetDAO> =
            pets.findAll().toList()

    fun getAllPetsBySpecies(species: String) : List<PetDAO> =
            pets.findBySpecies(species)

    fun getPet(id:Long): PetDAO =
            pets.findById(id).orElseThrow{NotFoundException("Pet with id $id not found.")}

    fun addPet(pet: PetDAO): PetDAO {
        if (pet.pet_id != 0L)
            throw PreconditionFailedException("Id must be 0 in insertion")
        else
            return pets.save(pet)
    }

    fun deletePet(id: Long) =
            pets.delete(getPet(id))

    fun updatePet (id: Long, newPet: PetDAO) {
        val current: PetDAO = getPet(id)
        current.update(newPet)
        pets.save(current)
    }

    fun getPetAppointments(id: Long): List<AppointmentDAO> {
        return appointments.findPetAppointments(id)
    }

    fun addAppointmentToPet(id: Long, appointment: AppointmentDAO) {
        if(appointment.appt_id != 0L)
            throw PreconditionFailedException("Id must be 0 in insertion")
        else {
            appointment.pet = getPet(id)
            appointments.save(appointment)
        }
    }
}