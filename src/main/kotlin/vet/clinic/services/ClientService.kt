package vet.clinic.services

import org.springframework.stereotype.Service
import vet.clinic.exceptions.NotFoundException
import vet.clinic.exceptions.PreconditionFailedException
import vet.clinic.model.*

@Service
class ClientService (val clients: ClientRepository,
                     val pets: PetRepository,
                     val appts: AppointmentRepository)
{

    fun getClient(id:Long): ClientDAO =
            clients.findById(id).orElseThrow{ NotFoundException("No client with id $id") }

    fun addClient(client: ClientDAO) {
        if (client.id != 0L)
            throw PreconditionFailedException("Id must be 0 in insertion")
        else
            clients.save(client)
    }

    fun deleteClient(id: Long) = clients.delete(getClient(id))

    fun updateClient(id: Long, newClient: ClientDAO) {
        val current: ClientDAO = getClient(id)
        current.update(newClient)
        clients.save(current)
    }

    fun getPetsByClientUsername(username:String): List<PetDAO> {
        val client:ClientDAO = getClientByUsername(username)
        return pets.findPetsByClientID(client.id)
    }

    fun getAppointmentsByClientUsername(username: String): List<AppointmentDAO> {
        val client:ClientDAO = getClientByUsername(username)
        return appts.findClientAppointments(client.id)
    }

    fun getClientByUsername(username:String):ClientDAO {
        return clients.findByUsername(username)
    }
}