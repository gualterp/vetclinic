package vet.clinic.services

import org.springframework.stereotype.Service
import vet.clinic.exceptions.NotFoundException
import vet.clinic.exceptions.PreconditionFailedException
import vet.clinic.model.*

@Service
class AdministrativeService (val admins: AdministrativeRepository, val employees: EmployeeRepository){

    fun getAllAdmins(): List<AdministrativeDAO> =
            admins.findAll().toList()

    fun getAdmin(id: Long): AdministrativeDAO =
            admins.findById(id)
            .orElseThrow { NotFoundException("Administrative with $id not found.") }

    fun addAdmin(admin: AdministrativeDAO) {
        if (admin.id != 0L)
            throw PreconditionFailedException("Id must be 0 in insertion")
        else{
            admins.save(admin)
            employees.save(admin)
        }
    }

    fun deleteAdmin(id: Long) =
            admins.delete(getAdmin(id))

    fun updateAdmin(id: Long, new_admin: AdministrativeDAO) {
        val current: AdministrativeDAO = getAdmin(id)
        current.update(new_admin)
        admins.save(current)
    }

    fun findEmployees():List<EmployeeDAO> =
            employees.findAll().toList()
}