package vet.clinic.services

import org.springframework.stereotype.Service
import vet.clinic.exceptions.NotFoundException
import vet.clinic.exceptions.PreconditionFailedException
import vet.clinic.exceptions.VetAlreadyFrozenException
import vet.clinic.model.*
import java.util.*

@Service
class VeterinarianService (val vets: VeterinarianRepository,
                           val appointments: AppointmentRepository)
{

    fun getAllVets():List<VeterinarianDAO> =
            vets.findAll().toList()

    fun getVet(id: Long): VeterinarianDAO =
            vets.findById(id).orElseThrow {NotFoundException("Veterinarian not found.")}

    fun addVet(vet: VeterinarianDAO): Long {
        if (vet.id != 0L)
            throw PreconditionFailedException("Id must be 0 in insertion")
        else
            return vets.save(vet).id
    }

    fun deleteVet(id: Long) {
        var vet = getVet(id)
        if(vet.frozen)
            throw VetAlreadyFrozenException("This vet was already fired.")
        else {
            vet.frozen = true
            vet.update(vet)
        }
    }

    fun updateVet(id: Long, new_vet: VeterinarianDAO) {
        val current: VeterinarianDAO = getVet(id)
        current.update(new_vet)
        vets.save(current)
    }

    fun getAppointmentsByUsername(username: String): List<AppointmentDAO> {
        val vet = getVetByUsername(username)
        return appointments.findVetAppointments(vet.id)
    }

    fun getVetAppointmentsOnRange(id: Long, start_date: Date, end_date: Date) : List<AppointmentDAO> {
        return appointments.findAppointmentsOfVetInRange(id, start_date, end_date)
    }

    fun getVetByUsername(username: String): VeterinarianDAO {
        return vets.findByUsername(username)
    }
}