package vet.clinic.services

import javassist.NotFoundException
import org.springframework.stereotype.Service
import vet.clinic.exceptions.PreconditionFailedException
import vet.clinic.model.AppointmentDAO
import vet.clinic.model.AppointmentRepository
import vet.clinic.model.PetDAO


@Service
class AppointmentService (val appointments: AppointmentRepository,
                          val pets: PetService)
{

    fun getAllAppointments(): List<AppointmentDAO> =
            appointments.findAll().toList()

    fun getAppointment(id: Long): AppointmentDAO =
            appointments.findById(id).orElseThrow { NotFoundException("Appointment with id $id not found.") }

    fun addAppointment(appt: AppointmentDAO, pet_id: Long) {
        if (appt.appt_id != 0L)
            throw PreconditionFailedException("Id must be 0 in insertion.")
        else {
            val pet: PetDAO = pets.getPet(pet_id)
            pets.updatePet(pet.pet_id, pet)
            appointments.save(appt)
        }
    }

    fun deleteAppointment(id:Long) =
            appointments.delete(getAppointment(id))

    fun updateAppointment(appointment: AppointmentDAO, id:Long) {
        val current: AppointmentDAO = getAppointment(id)
        current.update(appointment)
        appointments.save(current)
    }

    fun completeAppointment(id: Long) {
        val appt = getAppointment(id)
        val new_apt = AppointmentDAO(0L, appt.start_date, appt.end_date, appt.start_time,
                                      appt.end_time, appt.vet, appt.pet,
                                      appt.client,appt.description, true)
        appointments.delete(appt)
        appointments.save(new_apt)
    }
}