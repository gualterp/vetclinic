package vet.clinic.exceptions

class PreconditionFailedException(m:String) : RuntimeException(m)