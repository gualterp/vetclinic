package vet.clinic.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus
import java.lang.RuntimeException

@ResponseStatus(HttpStatus.CONFLICT)
class VetAlreadyFrozenException (msg:String) : RuntimeException(msg)