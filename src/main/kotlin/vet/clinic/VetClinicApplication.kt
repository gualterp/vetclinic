package vet.clinic

import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import vet.clinic.model.*
import java.time.LocalDateTime
import java.time.Month
import java.time.format.DateTimeFormatter

@SpringBootApplication
class VetClinicApplication {
    fun main(args: Array<String>) {
        runApplication<VetClinicApplication>(*args)
    }
}

