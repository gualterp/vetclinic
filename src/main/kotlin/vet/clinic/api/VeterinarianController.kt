package vet.clinic.api

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.web.bind.annotation.*
import vet.clinic.model.AppointmentDTO
import vet.clinic.model.ClientAppointmentDTO
import vet.clinic.model.VeterinarianDAO
import vet.clinic.model.VeterinarianDTO
import vet.clinic.services.AppointmentService
import vet.clinic.services.ClientService
import vet.clinic.services.PetService
import vet.clinic.services.VeterinarianService
import java.util.*

@Api(value = "VetClinic Management and Scheduling System - Veterinarian API",
        description = "Veterinarian Management Operations")

@RestController
@RequestMapping("/vets")
class VeterinarianController (val vet_service: VeterinarianService,
                              val pet_service: PetService,
                              val appt_service: AppointmentService,
                              val client_service: ClientService)
{

    // ---------------- Operation ----------------
    @ApiOperation("Add a new veterinarian.")
    @ApiResponses( value = [
        ApiResponse(code = 200, message = "Successfully added the new veterinarian."),
        ApiResponse(code = 401, message = "Authentication is required to proceed."),
        ApiResponse(code = 403, message = "Permission denied to proceed."),
        ApiResponse(code = 409, message = "Veterinarian already exists.")
    ])
    @PostMapping("")
    fun addVeterinarian(@RequestBody vet: VeterinarianDTO) =
            vet_service.addVet(VeterinarianDAO(vet))

    // ---------------- Operation ----------------
    @ApiOperation("Obtain all appointments for a given veterinarian.")
    @ApiResponses( value = [
        ApiResponse(code = 200, message = "Successfully returned all appointments for a given veterinarian."),
        ApiResponse(code = 401, message = "Authentication is required to access this resource."),
        ApiResponse(code = 403, message = "Permission denied to access this resource."),
        ApiResponse(code = 404, message = "No appointments found for the given veterinarian.")
    ])
    @GetMapping("/appointments/{vet_username}")
    fun getAppointments(@PathVariable vet_username:String): List<ClientAppointmentDTO> =
            vet_service.getAppointmentsByUsername(vet_username).map {
                ClientAppointmentDTO(it.appt_id, it.start_date, it.end_date, it.start_time, it.end_time,
                        vet_service.getVet(it.vet.id).name,
                        pet_service.getPet(it.pet.pet_id).name,
                        client_service.getClient(it.client.id).name, it.description, it.completed)
            }

    // ---------------- Operation ----------------
    @ApiOperation("Obtain all existing appointments on a given range for a veterinarian.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully returned all appointments on a given range."),
        ApiResponse(code = 401, message = "Authentication is required to access this resource."),
        ApiResponse(code = 403, message = "Permission denied to access this resource."),
        ApiResponse(code = 404, message = "No appointments found.")
    ])
    @GetMapping("/{vet_id}/appointments")
    fun getAllAppointmentsOnRange(@PathVariable vet_id: Long,
                                  @RequestBody start_date: Date,
                                  @RequestBody end_date: Date) =
            vet_service.getVetAppointmentsOnRange(vet_id, start_date, end_date).map{
                AppointmentDTO(it.appt_id, it.start_date, it.end_date, it.start_time, it.end_time,
                        it.vet.id, it.pet.pet_id, it.client.id, it.description, it.completed)
                }

    // ---------------- Operation ----------------
    @ApiOperation("Obtain all existing veterinarians.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully returned all existing veterinarians."),
        ApiResponse(code = 401, message = "Authentication is required to access this resource."),
        ApiResponse(code = 403, message = "Permission denied to access this resource."),
        ApiResponse(code = 404, message = "No veterinarians found.")
    ])
    @GetMapping("")
    fun getVeterinarians() = vet_service.getAllVets().map {
        VeterinarianDTO(it.id, it.name, it.picture, it.username, it.password,
                it.email, it.phone, it.address, it.role)
    }

    // ---------------- Operation ----------------
    @ApiOperation("Update a given veterinarian's information.")
    @ApiResponses( value = [
        ApiResponse(code = 200, message = "Successfully updated the given veterinarian."),
        ApiResponse(code = 401, message = "Authentication is required to access this resource."),
        ApiResponse(code = 403, message = "Permission denied to access this resource."),
        ApiResponse(code = 404, message = "The given veterinarian could not be found.")
    ])
    @PutMapping("{id}")
    fun updateVet(@RequestBody vet: VeterinarianDTO, @PathVariable id:Long) =
            vet_service.updateVet(id, VeterinarianDAO(vet))

    // ---------------- Operation ----------------
    @ApiOperation("Complete a given appointment.")
    @ApiResponses( value = [
        ApiResponse(code = 200, message = "Successfully completed the appointment."),
        ApiResponse(code = 401, message = "Authentication is required to access this resource."),
        ApiResponse(code = 403, message = "Permission denied to access this resource."),
        ApiResponse(code = 404, message = "The given appointment could not be found.")
    ])
    @PutMapping("/appointments/{appt_id}")
    fun completeAppointment(@PathVariable appt_id: Long) =
            appt_service.completeAppointment(appt_id)

    // ---------------- Operation ----------------
    @ApiOperation("Remove a given veterinarian.")
    @ApiResponses( value = [
        ApiResponse(code = 200, message = "Successfully deleted the veterinarian."),
        ApiResponse(code = 401, message = "Authentication is required to access this resource."),
        ApiResponse(code = 403, message = "Permission denied to access this resource."),
        ApiResponse(code = 404, message = "The given veterinarian could not be found.")
    ])
    @DeleteMapping("/{id}")
    fun deleteVet(@PathVariable id:Long) = vet_service.deleteVet(id)
}