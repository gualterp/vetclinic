package vet.clinic.api

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.web.bind.annotation.*
import vet.clinic.model.*
import vet.clinic.services.AppointmentService
import vet.clinic.services.ClientService
import vet.clinic.services.PetService
import vet.clinic.services.VeterinarianService

@Api(value = "VetClinic Management and Scheduling System - Client API",
        description = "Client Management Operations")

@RestController
@RequestMapping("/clients")
class ClientController (val pet_service: PetService, val appt_service: AppointmentService,
                        val vet_service: VeterinarianService, val client_service: ClientService)
{

    // ---------------- Operation ----------------
    @ApiOperation("Add a new client.")
    @ApiResponses( value = [
        ApiResponse(code = 200, message = "Successfully added the client."),
        ApiResponse(code = 401, message = "Authentication is required to proceed."),
        ApiResponse(code = 403, message = "Permission denied to proceed."),
        ApiResponse(code = 409, message = "Client already exists.")
    ])
    @PostMapping("")
    fun addClient(@RequestBody client: ClientDTO) =
            client_service.addClient(ClientDAO(client))

    // ---------------- Operation ----------------
    @ApiOperation("Add a new pet to a given client.")
    @ApiResponses( value = [
        ApiResponse(code = 200, message = "Successfully added the pet to the given client."),
        ApiResponse(code = 401, message = "Authentication is required to proceed."),
        ApiResponse(code = 403, message = "Permission denied to proceed."),
        ApiResponse(code = 409, message = "Client already owns the pet.")
    ])
    @PostMapping("/{id}/pets")
    fun addPet(@PathVariable id:Long, @RequestBody pet: PetDTO) =
            pet_service.addPet(PetDAO(pet, client_service.getClient(id)))

    // ---------------- Operation ----------------
    @ApiOperation("Obtain all pets for a given client.")
    @ApiResponses( value = [
        ApiResponse(code = 200, message = "Successfully returned all pets for the given client."),
        ApiResponse(code = 401, message = "Authentication is required to access this resource."),
        ApiResponse(code = 403, message = "Permission denied to access this resource."),
        ApiResponse(code = 404, message = "No pets found for the given client.")
    ])
    @GetMapping("/{username}/pets")
    fun getPets(@PathVariable username:String):List<PetDTO> =
            client_service.getPetsByClientUsername(username).map {
                PetDTO(it.pet_id, it.name, it.species, it.age, it.client.username,
                        it.description, it.health_status, it.picture)
            }

    // ---------------- Operation ----------------
    @ApiOperation("Obtain all appointments for a given client.")
    @ApiResponses( value = [
        ApiResponse(code = 200, message = "Successfully returned all appointments for the given client."),
        ApiResponse(code = 401, message = "Authentication is required to access this resource."),
        ApiResponse(code = 403, message = "Permission denied to access this resource."),
        ApiResponse(code = 404, message = "No appointments found for the given client.")
    ])
    @GetMapping("/{username}/appointments")
    fun getAppointments(@PathVariable username: String):List<ClientAppointmentDTO> =
            client_service.getAppointmentsByClientUsername(username).map {
                ClientAppointmentDTO(it.appt_id, it.start_date, it.end_date, it.start_time, it.end_time,
                        vet_service.getVet(it.vet.id).name,
                        pet_service.getPet(it.pet.pet_id).name,
                        client_service.getClient(it.client.id).name, it.description, it.completed
                )
            }

    // ---------------- Operation ----------------
    @ApiOperation("Update a given client's information.")
    @ApiResponses( value = [
        ApiResponse(code = 200, message = "Successfully updated the client."),
        ApiResponse(code = 401, message = "Authentication is required to access this resource."),
        ApiResponse(code = 403, message = "Permission denied to access this resource."),
        ApiResponse(code = 404, message = "The given client could not be found.")
    ])
    @PutMapping("/{id}")
    fun updateClient(@PathVariable id:Long, @RequestBody client: ClientDTO) =
            client_service.updateClient(id, ClientDAO(client))

    // ---------------- Operation ----------------
    @ApiOperation("Remove a existing client.")
    @ApiResponses( value = [
        ApiResponse(code = 200, message = "Successfully removed the client."),
        ApiResponse(code = 401, message = "Authentication is required to proceed."),
        ApiResponse(code = 403, message = "Permission denied to proceed."),
        ApiResponse(code = 404, message = "The given client could not be found.")
    ])
    @DeleteMapping("/{id}")
    fun deleteClient(@PathVariable id:Long) =
            client_service.deleteClient(id)
}