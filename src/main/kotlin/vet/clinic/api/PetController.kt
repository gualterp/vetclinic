package vet.clinic.api

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import javassist.NotFoundException
import org.springframework.web.bind.annotation.*
import vet.clinic.model.AppointmentDAO
import vet.clinic.model.AppointmentDTO
import vet.clinic.model.PetDAO
import vet.clinic.model.PetDTO
import vet.clinic.services.ClientService
import vet.clinic.services.PetService
import vet.clinic.services.VeterinarianService

@Api(value = "VetClinic Management and Scheduling System - Pet API",
        description = "Pet Management Operations")

@RestController
@RequestMapping("/pets")
class PetController(val pet_service: PetService,
                    val client_service: ClientService,
                    val vet_service: VeterinarianService)
{

    // ---------------- Operation ----------------
    @ApiOperation("Add a new pet.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully added  and retrieved the pet."),
        ApiResponse(code = 401, message = "Authentication is required to proceed."),
        ApiResponse(code = 403, message = "Permission denied to proceed."),
        ApiResponse(code = 409, message = "Pet already exists.")
    ])
    @PostMapping("")
    fun addPet(@RequestBody pet: PetDTO): PetDTO =
            PetDTO(pet_service.addPet(PetDAO(pet,
                    client_service.getClientByUsername(pet.client_username))))

    // ---------------- Operation ----------------
    @ApiOperation("Obtain all existing pets, optionally filtered by species.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully returned all existing pets."),
        ApiResponse(code = 401, message = "Authentication is required to access this resource."),
        ApiResponse(code = 403, message = "Permission denied to access this resource."),
        ApiResponse(code = 404, message = "No pets found.")
    ])
    @GetMapping("")
    fun getAllPets(@RequestParam(required = false) species: String?):List<PetDTO> =
            species?.let{
                pet_service.getAllPetsBySpecies(species)}
                    ?: pet_service.getAllPets().map {
                        PetDTO(it.pet_id, it.name, it.species, it.age, it.client.username,
                                it.description, it.health_status, it.picture)
                    }

    // ---------------- Operation ----------------
    @ApiOperation("Obtain the details of a given pet.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully returned the pet information."),
        ApiResponse(code = 401, message = "Authentication is required to access this resource."),
        ApiResponse(code = 403, message = "Permission denied to access this resource."),
        ApiResponse(code = 404, message = "The given pet could not be found.")
    ])
    @GetMapping("/{id}")
    fun getPet(@PathVariable id: Long) = PetDTO(pet_service.getPet(id))

    // ---------------- Operation ----------------
    @ApiOperation("Obtain the appointments of a given pet.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully returned the pet's appointments."),
        ApiResponse(code = 401, message = "Authentication is required to proceed."),
        ApiResponse(code = 403, message = "Permission denied to proceed."),
        ApiResponse(code = 404, message = "The given pet could not be found.")
    ])
    @GetMapping("/{id}/appointments")
    fun getPetAppointments(@PathVariable id: Long) =
            pet_service.getPetAppointments(id).map {
                AppointmentDTO(it.appt_id, it.start_date, it.end_date, it.start_time, it.end_time,
                        it.vet.id, it.pet.pet_id, it.client.id, it.description, it.completed)
            }

    // ---------------- Operation ----------------
    @ApiOperation("Update a given pet's information.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully updated the pet information."),
        ApiResponse(code = 401, message = "Authentication is required to proceed."),
        ApiResponse(code = 403, message = "Permission denied to proceed."),
        ApiResponse(code = 404, message = "The given pet could not be found.")
    ])
    @PutMapping("/{id}")
    fun updatePet(@RequestBody pet: PetDTO, @PathVariable id: Long) {
        try {
            pet_service.updatePet(id, PetDAO(pet, client_service.getClientByUsername(pet.client_username)))
        } catch (e: NotFoundException) {
            throw NotFoundException(e.message ?: "Pet not found.")
        }
    }

    // ---------------- Operation ----------------
    @ApiOperation("Remove a existing pet.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully removed the pet."),
        ApiResponse(code = 401, message = "Authentication is required to proceed."),
        ApiResponse(code = 403, message = "Permission denied to proceed."),
        ApiResponse(code = 404, message = "The desired pet was not found.")
    ])
    @DeleteMapping("/{id}")
    fun deletePet(@PathVariable id: Long) {
        try {
            pet_service.deletePet(id)
        } catch (e: NotFoundException) {
            throw NotFoundException(e.message ?: "Pet not found.")
        }
    }

    // ---------------- Operation ----------------
    @ApiOperation("Add a new appointment to an existing pet.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully added the appointment."),
        ApiResponse(code = 401, message = "Authentication is required to proceed."),
        ApiResponse(code = 403, message = "Permission denied to proceed."),
        ApiResponse(code = 404, message = "The desired pet was not found.")
    ])
    @PostMapping("/{id}/appointments")
    fun addAppointmentToPet(@PathVariable id: Long, @RequestBody appt: AppointmentDTO) =
            pet_service.addAppointmentToPet(id, AppointmentDAO(appt,
                    vet_service.getVet(appt.vet_id), pet_service.getPet(id),
                    client_service.getClient(appt.client_id))
            )
}