package vet.clinic.api

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.web.bind.annotation.*
import vet.clinic.exceptions.NotFoundException
import vet.clinic.model.AppointmentDAO
import vet.clinic.model.AppointmentDTO
import vet.clinic.model.ClientAppointmentDTO
import vet.clinic.services.AppointmentService
import vet.clinic.services.ClientService
import vet.clinic.services.PetService
import vet.clinic.services.VeterinarianService

@Api(value = "VetClinic Management and Scheduling System - Appointments API",
        description = "Appointment Management Operations")

@RestController
@RequestMapping("/appointments")
class AppointmentController (
        val appt_service: AppointmentService,
        val vet_service: VeterinarianService,
        val pet_service: PetService,
        val client_service: ClientService)
{

    // ---------------- Operation ----------------
    @ApiOperation("Add a new appointment.")
    @ApiResponses( value = [
        ApiResponse(code = 200, message = "Successfully added the appointment."),
        ApiResponse(code = 401, message = "Authentication is required to proceed."),
        ApiResponse(code = 403, message = "Permission denied to proceed."),
        ApiResponse(code = 409, message = "Appointment already exists.")
    ])
    @PostMapping("")
    fun addAppointment(@RequestBody appt: AppointmentDTO) = appt_service.addAppointment(
            AppointmentDAO(appt, vet_service.getVet(appt.vet_id), pet_service.getPet(appt.pet_id),
                    client_service.getClient(appt.client_id)), appt.pet_id)

    // ---------------- Operation ----------------
    @ApiOperation("Obtain all existing appointments.")
    @ApiResponses( value = [
        ApiResponse(code = 200, message = "Successfully returned all existing appointments."),
        ApiResponse(code = 401, message = "Authentication is required to access this resource."),
        ApiResponse(code = 403, message = "Permission denied to access this resource."),
        ApiResponse(code = 404, message = "No appointments found.")
    ])
    @GetMapping("")
    fun getAllAppointments() = appt_service.getAllAppointments().map {
        ClientAppointmentDTO(it.appt_id, it.start_date, it.end_date, it.start_time, it.end_time,
                vet_service.getVet(it.vet.id).name,
                pet_service.getPet(it.pet.pet_id).name,
                client_service.getClient(it.client.id).name, it.description, it.completed)
    }

    // ---------------- Operation ----------------
    @ApiOperation("Obtain the details of a given appointment.")
    @ApiResponses( value = [
        ApiResponse(code = 200, message = "Successfully returned the given appointment details."),
        ApiResponse(code = 401, message = "Authentication is required to access this resource."),
        ApiResponse(code = 403, message = "Permission denied to access this resource."),
        ApiResponse(code = 404, message = "The given appointment could not be found.")
    ])
    @GetMapping("/{id}")
    fun getAppointment(@PathVariable id:Long) =
            AppointmentDTO(appt_service.getAppointment(id))

    // ---------------- Operation ----------------
    @ApiOperation("Update a given appointment.")
    @ApiResponses( value = [
        ApiResponse(code = 200, message = "Successfully updated the appointment."),
        ApiResponse(code = 401, message = "Authentication is required to proceed."),
        ApiResponse(code = 403, message = "Permission denied to proceed."),
        ApiResponse(code = 404, message = "Appointment not found.")
    ])
    @PutMapping("")
    fun updateAppointment(@RequestBody appt: AppointmentDTO, @PathVariable id: Long) {
        try {
            appt_service.updateAppointment(AppointmentDAO(appt, vet_service.getVet(appt.vet_id),
                    pet_service.getPet(appt.pet_id), client_service.getClient(appt.client_id)), id)
        } catch(e: NotFoundException){
            throw NotFoundException(e.message ?: "Appointment not found.")
        }
    }

    // ---------------- Operation ----------------
    @ApiOperation("Remove an appointment.")
    @ApiResponses( value = [
        ApiResponse(code = 200, message = "Successfully removed the appointment."),
        ApiResponse(code = 401, message = "Authentication is required to proceed."),
        ApiResponse(code = 403, message = "Permission denied to proceed."),
        ApiResponse(code = 404, message = "Appointment not found.")
    ])
    @DeleteMapping("/{id}")
    fun deleteAppointment(@PathVariable id:Long) = appt_service.deleteAppointment(id)
}