package vet.clinic.api

import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.web.bind.annotation.*
import vet.clinic.model.*
import vet.clinic.services.*

@Api(value = "VetClinic Management and Scheduling System - Administrative API",
        description = "Administrative Management Operations")

@RestController
@RequestMapping("/admin")
class AdministrativeController (
        val admin_service: AdministrativeService,
        val vet_service: VeterinarianService,
        val client_service: ClientService)
{

    // ---------------- Operation ----------------
    @ApiOperation("Add a new administrative.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully added the new administrative."),
        ApiResponse(code = 401, message = "Authentication is required to proceed."),
        ApiResponse(code = 403, message = "Permission denied to proceed."),
        ApiResponse(code = 409, message = "Administrative already exists.")
    ])
    @PostMapping("")
    fun addAdministrative(@RequestBody admin: AdministrativeDTO) =
            admin_service.addAdmin(AdministrativeDAO(admin))

    // ---------------- Operation ----------------
    @ApiOperation("Obtain all existing employees.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully returned all existing employees."),
        ApiResponse(code = 401, message = "Authentication is required to access this resource."),
        ApiResponse(code = 403, message = "Permission denied to access this resource."),
        ApiResponse(code = 404, message = "No employees found.")
    ])
    @GetMapping("/employees")
    fun getEmployees() = admin_service.findEmployees().map {
        EmployeeDTO(it.id, it.name, it.picture, it.username, it.password, it.email, it.phone, it.address, it.role)
    }

    // ---------------- Operation ----------------
    @ApiOperation("Obtain all existing administratives.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully returned all existing administratives."),
        ApiResponse(code = 401, message = "Authentication is required to access this resource."),
        ApiResponse(code = 403, message = "Permission denied to access this resource."),
        ApiResponse(code = 404, message = "No administratives found.")
    ])
    @GetMapping("")
    fun getAdministratives() = admin_service.getAllAdmins().map {
        AdministrativeDTO(it.id, it.name, it.picture, it.username, it.password, it.email, it.phone, it.address, it.role)
    }

    // ---------------- Operation ----------------
    @ApiOperation("Update an administrative information.")
    @ApiResponses( value = [
        ApiResponse(code = 200, message = "Successfully updated the administrative."),
        ApiResponse(code = 401, message = "Authentication is required to access this resource."),
        ApiResponse(code = 403, message = "Permission denied to access this resource."),
        ApiResponse(code = 404, message = "The given administrative could not found.")
    ])
    @PutMapping("{id}")
    fun updateAdministrative(@RequestBody admin: AdministrativeDTO, @PathVariable id:Long) =
            admin_service.updateAdmin(id, AdministrativeDAO(admin))

    // ---------------- Operation ----------------
    @ApiOperation("Delete an existing administrative.")
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully removed the administrative."),
        ApiResponse(code = 401, message = "Authentication is required to proceed."),
        ApiResponse(code = 403, message = "Permission denied to proceed."),
        ApiResponse(code = 404, message = "The given administrative could not be found.")
    ])
    @DeleteMapping("/{id}")
    fun deleteAdministrative(@PathVariable id:Long) = admin_service.deleteAdmin(id)
}