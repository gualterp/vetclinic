package vet.clinic.model

data class PetDTO (
        var pet_id: Long,
        var name: String,
        var species: String,
        var age: Int,
        var client_username: String,
        var description: String,
        var health_status: String,
        var picture: String
)
{
    constructor(pet: PetDAO) : this(pet.pet_id, pet.name, pet.species, pet.age, pet.client.username,
                                    pet.description, pet.health_status, pet.picture)
}

data class AppointmentDTO (
        val appt_id: Long,
        val start_date: String,
        val end_date: String,
        var start_time: String,
        var end_time: String,
        val vet_id: Long,
        val pet_id: Long,
        val client_id: Long,
        val description: String,
        val completed: Boolean
) {
    constructor(apt: AppointmentDAO): this(
            apt.appt_id, apt.start_date, apt.end_date,
            apt.start_time, apt.end_time, apt.vet.id, apt.pet.pet_id,
            apt.client.id, apt.description, apt.completed)
}

data class ClientAppointmentDTO (
        val appt_id: Long,
        val start_date: String,
        val end_date: String,
        val start_time: String,
        val end_time: String,
        val vet_name: String,
        val pet_name: String,
        val client_name: String,
        val description: String,
        val completed: Boolean
)

data class ClientDTO (
        var client_id:Long,
        var name: String,
        var picture: String,
        var username: String,
        var password: String,
        var email: String,
        var phone: Number,
        var address: String,
        var role: String
)

data class VeterinarianDTO (
        var vet_id:Long,
        var name: String,
        var picture: String,
        var username: String,
        var password: String,
        var email: String,
        var phone: Number,
        var address: String,
        var role: String
)

data class AdministrativeDTO (
        var admin_id:Long,
        var name: String,
        var picture: String,
        var username: String,
        var password: String,
        var email: String,
        var phone: Number,
        var address: String,
        var role: String
)

data class EmployeeDTO (
        var employee_id:Long,
        var name: String,
        var picture: String,
        var username: String,
        var password: String,
        var email: String,
        var phone: Number,
        var address: String,
        var role: String
)