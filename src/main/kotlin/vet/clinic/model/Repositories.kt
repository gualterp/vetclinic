package vet.clinic.model

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import java.util.*

interface PetRepository : JpaRepository<PetDAO, Long> {
    // Finds pets with a given species
    @Query("select p from PetDAO p where p.species = :species")
    fun findBySpecies(@Param("species") species: String): List<PetDAO>

    // Gets the pets of a given client
    @Query("select p from PetDAO p join p.client c where c.id = :id")
    fun findPetsByClientID(id: Long): List<PetDAO>
}

interface AppointmentRepository : JpaRepository<AppointmentDAO, Long> {
    // Gets the appointments of a given pet
    @Query("select a from AppointmentDAO a join a.pet p where p.pet_id = :id")
    fun findPetAppointments(id: Long) : List<AppointmentDAO>

    // Gets the appointments of a given client
    @Query("select a from AppointmentDAO a join a.client c where c.id = :id")
    fun findClientAppointments(id: Long) : List<AppointmentDAO>

    // Gets the appointments of a given veterinarian
    @Query("select a from AppointmentDAO a join a.vet v where v.id = :id")
    fun findVetAppointments(id: Long) : List<AppointmentDAO>

    // Gets the appointments of a given veterinarian, in a given range
    @Query("select a from AppointmentDAO a join a.vet v where v.id = :id " +
            "and a.start_date between :start_date and :end_date")
    fun findAppointmentsOfVetInRange(id: Long, start_date: Date, end_date: Date): List<AppointmentDAO>
}

interface ClientRepository : JpaRepository<ClientDAO, Long> {

    @Query("select c from ClientDAO c where c.name=:name")
    fun findByName(name:String): ClientDAO

    fun findByUsername(username:String):ClientDAO
}

interface VeterinarianRepository : JpaRepository<VeterinarianDAO, Long> {
    fun findByUsername(username:String): VeterinarianDAO
}

interface AdministrativeRepository : JpaRepository<AdministrativeDAO, Long>

interface EmployeeRepository : JpaRepository<EmployeeDAO, Long>




