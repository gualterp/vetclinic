package vet.clinic.model

import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction
import javax.persistence.*
import kotlin.reflect.full.createInstance

@Entity
data class PetDAO (
        @Id @GeneratedValue
        val pet_id: Long,
        var name: String,
        var species: String,
        var age: Int,

        @ManyToOne(fetch = FetchType.LAZY, optional = true)
        @JoinColumn(name = "client_id", nullable = false)
        @OnDelete(action = OnDeleteAction.CASCADE)
        var client: ClientDAO,

        var description: String,
        var health_status: String,
        var picture: String,
        var medical_record: String
)
{
    constructor(pet: PetDTO, client:ClientDAO): this(pet.pet_id, pet.name, pet.species, pet.age, client,
                pet.description, pet.health_status, pet.picture, "")

    constructor() : this(0L, "", "", 0, ClientDAO(),
            "", "", "", "")

    fun update (pet:PetDAO) {
        this.name = pet.name
        this.species = pet.species
        this.age = pet.age
        this.client = pet.client
        this.description = pet.description
        this.health_status = pet.health_status
        this.picture = pet.picture
        this.medical_record = pet.medical_record
    }
}

@Entity
data class AppointmentDAO(
        @Id @GeneratedValue
        val appt_id: Long,
        var start_date: String,
        var end_date: String,
        var start_time: String,
        var end_time: String,

        @ManyToOne(fetch = FetchType.LAZY, optional = false)
        @JoinColumn(name ="vet_id", nullable = false)
        @OnDelete(action = OnDeleteAction.CASCADE)
        var vet: VeterinarianDAO,

        @ManyToOne(fetch = FetchType.LAZY, optional = false)
        @JoinColumn(name ="pet_id", nullable = false)
        @OnDelete(action = OnDeleteAction.CASCADE)
        var pet: PetDAO,

        @ManyToOne(fetch = FetchType.LAZY, optional = false)
        @JoinColumn(name ="client_id", nullable = false)
        @OnDelete(action = OnDeleteAction.CASCADE)
        var client: ClientDAO,
        var description: String,
        var completed: Boolean
)
{
    constructor(appt:  AppointmentDTO, vet:VeterinarianDAO, pet:PetDAO, client: ClientDAO):
            this(appt.appt_id, appt.start_date, appt.end_date, appt.start_time, appt.end_time,
                    vet, pet, client, appt.description, appt.completed)

    constructor() : this(0L, "", "", "", "",
            VeterinarianDAO(), PetDAO(), ClientDAO(), "", false)

    fun update(appt: AppointmentDAO) {
        this.start_date = appt.start_date
        this.end_date = appt.end_date
        this.vet = appt.vet
        this.pet = appt.pet
        this.client = appt.client
        this.description = appt.description
        this.completed = appt.completed
    }
}

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
open class UserDAO (
        @Id @GeneratedValue
        open val id:Long,
        open var name: String,
        open var picture: String,
        @Column(unique=true)
        open var username: String,
        open var password: String,
        open var email: String,
        open var phone: Number,
        open var address: String,
        open var role: String
) {
    constructor() : this(0L, "", "", "","",
            "", 0,"", "")
}

@Entity
class ClientDAO (
        id: Long,
        name: String,
        picture: String,
        username: String,
        password: String,
        email: String,
        phone: Number,
        address: String,
        role: String
): UserDAO( id, name, picture, username, password, email, phone, address, role)
{
    constructor(client: ClientDTO): this(client.client_id, client.name,
            client.picture, client.username, client.password, client.email, client.phone, client.address, client.role)

    constructor() : this(0L, "", "", "", "",
            "", 0, "", "")

    fun update(client: ClientDAO) {
        this.name = client.name
        this.picture = client.picture
        this.password = client.password
        this.email = client.email
        this.phone = client.phone
        this.address = client.address
        this.role = client.role
    }
}

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
open class EmployeeDAO (
        id: Long,
        name: String,
        picture: String,
        username: String,
        password: String,
        email: String,
        phone: Number,
        address: String,
        role: String
) : UserDAO( id, name, picture, username, password, email, phone, address, role )
{
    constructor() : this(0L, "", "", "", "",
            "", 0, "", "")
}

@Entity
class VeterinarianDAO (
        var frozen: Boolean,
        id: Long,
        name: String,
        picture: String,
        username: String,
        password: String,
        email: String,
        phone: Number,
        address: String,
        role: String
) : EmployeeDAO( id, name, picture, username, password, email, phone, address, role )
{

    constructor(vet: VeterinarianDTO) :
            this(false, vet.vet_id, vet.name, vet.picture,
                    vet.username, vet.password, vet.email, vet.phone, vet.address, vet.role)

    constructor() : this(false, 0L, "", "", "", "",
            "", 0,"", "")

    fun update(vet: VeterinarianDAO) {
        this.name = vet.name
        this.picture = vet.picture
        this.password = vet.password
        this.email = vet.email
        this.phone = vet.phone
        this.address = vet.address
        this.frozen = vet.frozen
        this.role = vet.role
    }
}

@Entity
class AdministrativeDAO (
        id: Long,
        name: String,
        picture: String,
        username: String,
        password: String,
        email: String,
        phone: Number,
        address: String,
        role: String
) : EmployeeDAO( id, name, picture, username, password, email, phone, address, role )
{
    constructor(admin: AdministrativeDTO):
            this(admin.admin_id, admin.name, admin.picture, admin.username, admin.password, admin.email,
                    admin.phone, admin.address, admin.role)

    constructor() : this(0L, "", "", "", "",
            "", 0, "","")

    fun update(admin: AdministrativeDAO) {
        this.name = admin.name
        this.picture = admin.picture
        this.password = admin.password
        this.email = admin.email
        this.phone = admin.phone
        this.address = admin.address
        this.role = admin.role
    }
}